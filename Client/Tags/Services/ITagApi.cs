namespace StudyMate.Client.Tags.Services;

using RestEase;
using Shared.Models;

[BasePath("api/tags")]
public interface ITagApi
{
	[Get]
	public Task<List<string>> AllAsync();
}