namespace StudyMate.Client.Group.Services;

using AppShell;
using Microsoft.AspNetCore.SignalR.Client;
using Shared.Group.Models;
using Shared.Group.Services;
using Shared.Models;

public class GroupClient : IGroupClient, IGroupHub
{
	private readonly AppState state;
	private readonly HubConnection connection;
	private readonly IGroupApi restApi;

	private readonly Task startup;

	/// <summary>
	/// Fires when a new group was created on the server.
	/// </summary>
	public event Action<Group>? GroupCreated;

	/// <summary>
	/// Fires when existing group changed on the server.
	/// </summary>
	public event Action<Group>? GroupChanged;

	/// <summary>
	/// Just for testing purposes.
	/// </summary>
#pragma warning disable CS8618
	public GroupClient()
#pragma warning restore CS8618
	{
	}

	public GroupClient(AppState state, IRetryPolicy policy, IGroupApi restApi)
	{
		Console.WriteLine("Constructing GroupsClient");
		this.state = state;
		this.restApi = restApi;
		connection = BuildConnection(policy);
		startup = StartConnection();
		Console.WriteLine("GroupsClient constructed");
	}

	private HubConnection BuildConnection(IRetryPolicy policy)
	{
		var con = new HubConnectionBuilder()
			.WithUrl(state.BaseAddress + IGroupHub.Route)
			.WithAutomaticReconnect(policy)
			.AddMessagePackProtocol()
			.Build();
		con.Closed += exc =>
		{
			Console.WriteLine($"Websockets closed: {exc}");
			state.RtcConnected = false;
			return Task.CompletedTask;
		};
		con.Reconnecting += exc =>
		{
			Console.WriteLine($"Websockets reconnecting: {exc}");
			state.RtcConnected = false;
			return Task.CompletedTask;
		};
		con.Reconnected += msg =>
		{
			Console.WriteLine($"Websockets reconnected: {msg}");
			state.RtcConnected = true;
			return Task.CompletedTask;
		};


		con.On<Group>(nameof(OnGroupCreated), OnGroupCreated);
		con.On<Group>(nameof(OnGroupChanged), OnGroupChanged);
		con.On<ChatMessage>(nameof(IGroupClient.ReceiveMessage), ReceiveMessage);

		return con;
	}

	private async Task StartConnection()
	{
		await connection.StartAsync();
		await state.InitTask;
		state.RtcConnected = true;
		await OnLoginChanged();

		state.LoginChanged += async () => await OnLoginChanged();
		Console.WriteLine($"RTC connected: {connection.State}");
	}

	private async Task OnLoginChanged()
	{
		if (state.CurrentUser is not null)
		{
			await Login(state.Token);
		}
	}

	/// <summary>
	///	Hook called by framework. Subscribe to <see cref="GroupCreated"/> to get notified when this method is called.
	/// </summary>
	/// <param name="group">The group created on the server.</param>
	public Task OnGroupCreated(Group group)
	{
		Console.WriteLine(group.Id);
		GroupCreated?.Invoke(group);
		return Task.CompletedTask;
	}

	/// <summary>
	///	Hook called by framework. Subscribe to <see cref="GroupChanged"/> to get notified when this method is called.
	/// </summary>
	/// <param name="group">The group which changed on the server.</param>
	public Task OnGroupChanged(Group group)
	{
		GroupChanged?.Invoke(group);
		return Task.CompletedTask;
	}

	public Task ReceiveMessage(ChatMessage message)
	{
		OnMessage?.Invoke(message);
		return Task.CompletedTask;
	}

	public async Task<bool> Login(Guid token)
	{
		return await connection.InvokeAsync<bool>(nameof(IGroupHub.Login), token);
	}

	public async void Logout()
	{
		await connection.InvokeAsync(nameof(IGroupHub.Logout));
	}

	public async Task CreateGroup(NewGroupRequest newGroup)
	{
		try
		{
			await connection.InvokeAsync(nameof(IGroupHub.CreateGroup), newGroup);
		}
		catch (Exception exc)
		{
			Console.WriteLine($"CreateGroup via RTC failed: {exc.Message}");
			await restApi.CreateGroup(newGroup);
		}
	}

	public async Task JoinGroup(int groupId)
	{
		try
		{
			await connection.InvokeAsync(nameof(IGroupHub.JoinGroup), groupId);
		}
		catch (Exception exc)
		{
			Console.WriteLine($"Join group via RTC failed: {exc.Message}");
			//await restApi.CreateGroup(newGroup);
		}
	}
	public async Task SendMessage(ChatMessage message)
	{
		if (state.CurrentUser is not null)
		{
			message.Sender = state.CurrentUser.Username;
		}
		await connection.InvokeAsync(nameof(IGroupHub.SendMessage), message);
	}

	public event Action<ChatMessage>? OnMessage;
}
