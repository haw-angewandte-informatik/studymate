namespace StudyMate.Client.Group.Services;

using RestEase;
using Shared.Group.Models;
using Shared.Models;

[BasePath("api/group")]
public interface IGroupApi
{
	[Get]
	public Task<List<Group>> GetOpenGroups([Query] int numOfGroups = 20);

	[Post]
	public Task CreateGroup([Body] NewGroupRequest request);

	[Post]
	public Task JoinGroup([Query] int groupId);
}
