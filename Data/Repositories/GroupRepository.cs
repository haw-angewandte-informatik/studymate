namespace StudyMate.Data.Repositories;

using Microsoft.EntityFrameworkCore;
using Entities;
using Services;
using StudyMate.Shared.Group.Services;

public class GroupRepository : IGroupRepository
{
	private readonly IDbContextFactory<StudyMateContext> contextFactory;
	private readonly DatabaseEvents? events;

	public GroupRepository(IDbContextFactory<StudyMateContext> contextFactory, DatabaseEvents? events = null)
	{
		this.contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
		this.events = events;
	}

	public async Task<Shared.Models.Group?> FindAsync(int groupId)
	{
		await using var context = await contextFactory.CreateDbContextAsync();
		var groupEntity = await context.Groups
			.Include(g => g.Members).ThenInclude(m => m.ProfilePicture)
			.Include(g => g.Tags)
			.SingleOrDefaultAsync(g => g.Id == groupId);

		return groupEntity?.ToModel();
	}

	public async Task<Shared.Models.Group> CreateGroup(Shared.Group.Models.NewGroupRequest group)
	{
		await using var context = await contextFactory.CreateDbContextAsync();
		var members = group.Members.Select(m => context.Users.Include(u => u.ProfilePicture).FirstOrDefault(u => u.Username == m.Username)).ToList();
		var tags = new List<Tag>();
		foreach (string tag in group.Tags)
		{
			var tagToAdd = context.Tags.FirstOrDefault(t => t.Name == tag);
			if (tagToAdd is null) // tag does not exist yet
			{
				context.Tags.Add(new Tag { Name = tag });
				await context.SaveChangesAsync();
				tagToAdd = context.Tags.FirstOrDefault(t => t.Name == tag);
			}
			tags.Add(tagToAdd!);
		}

		var groupEntity = new Group
		{
			OwnerUsername = group.Owner.Username,
			Name = group.Name,
			Capacity = (uint)group.Capacity,
			Tags = tags,
			Members = members,
		};
		context.Groups.Add(groupEntity);
		await context.SaveChangesAsync();

		events?.GroupCreated(groupEntity.ToModel());
		return groupEntity.ToModel();
	}

	public async Task<List<Shared.Models.Group>> GetOpenGroups(int numOfGroups)
	{
		await using var context = await contextFactory.CreateDbContextAsync();
		var groups = await context.Groups
			.AsNoTracking()
			.Include(g => g.Tags)
			.Include(g => g.Members).ThenInclude(m => m.ProfilePicture)
			.Where(g => g.Capacity > g.Members.Count) // nur offene Gruppen
			.Take(numOfGroups)
			.Select(g => g.ToModel())
			.ToListAsync();
		groups.Reverse(); // latest on top
		return groups;
	}

	public async Task<Shared.Models.Group> JoinGroup(int groupId, Guid userId)
	{
		await using var context = await contextFactory.CreateDbContextAsync();
		var group = await context.Groups
			.Include(g => g.Tags)
			.Include(g => g.Members).ThenInclude(m => m.ProfilePicture)
			.FirstAsync(g => g.Id == groupId);
		var token = await context.LoginTokens
			.Include(l => l.UserNavigation).ThenInclude(u => u.ProfilePicture)
			.FirstAsync(t => t.Token == userId);
		var user = token.UserNavigation;
		group.Members.Add(user);
		await context.SaveChangesAsync();
		events?.GroupChanged(group.ToModel());
		return group.ToModel();
	}
}