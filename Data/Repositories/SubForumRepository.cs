namespace StudyMate.Data.Repositories;

using Microsoft.EntityFrameworkCore;
using StudyMate.Shared.Services;

public class SubForumRepository : ISubForumRepository
{
	private readonly IDbContextFactory<StudyMateContext> _contextFactory;

	public SubForumRepository(IDbContextFactory<StudyMateContext> contextFactory)
	{
		_contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
	}

	public async Task<List<Shared.Models.Post>> GetLatestPosts(int numOfPosts)
	{
		using var context = await _contextFactory.CreateDbContextAsync();
		return await context.Posts
		 	.Include(p => p.Author).ThenInclude(a => a.ProfilePicture)
			.Include(p => p.Tags)
			.Include(p => p.SubForum)
			.OrderByDescending(p => p.Posted)
			.Take(numOfPosts)
			.Select(p => new Shared.Models.Post(
				p.Title, false,
				p.Content,
				p.Posted,
				p.SubForum.Title,
				p.Author.ToModel(),
				p.Tags.Select(t => t.Name).ToList()
			))
			.AsNoTracking()
			.ToListAsync();
	}
}