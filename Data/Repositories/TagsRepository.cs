namespace StudyMate.Data.Repositories;

using Microsoft.EntityFrameworkCore;
using StudyMate.Shared.Services;

public class TagsRepository : ITagsRepository
{
	private readonly IDbContextFactory<StudyMateContext> _contextFactory;

	public TagsRepository(IDbContextFactory<StudyMateContext> contextFactory)
	{
		_contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
	}

	public async Task<List<string>> AllAsync()
	{
		await using var context = await _contextFactory.CreateDbContextAsync();
		return await context.Tags
			.Select(t => t.Name)
			.OrderByDescending(t => t)
			.ToListAsync();
	}
}