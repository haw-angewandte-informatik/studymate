namespace StudyMate.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;
using Shared.Group.Services;
using Shared.Models;
using StudyMate.Shared.Services;

public static class Config
{
	public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration config, Action<IServiceProvider, Group>? onGroupCreatedCallback)
	{
		AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
		services.AddScoped<IUserRepository, UserRepository>();
		services.AddScoped<IGroupRepository, GroupRepository>();
		services.AddScoped<ITagsRepository, TagsRepository>();
		services.AddScoped<ISubForumRepository, SubForumRepository>();
#if DEBUG
		services.AddDbContextFactory<StudyMateContext>(opt =>
				opt.UseNpgsql(config.GetConnectionString("StudyMateContext"))
				.EnableSensitiveDataLogging());
#else
		services.AddDbContextFactory<StudyMateContext>(opt =>
			opt.UseNpgsql(config.GetConnectionString("StudyMateContext")));
#endif
		
		return services;
	}

	public static void UpdateDatabase(this IServiceProvider provider)
	{
		using var scope = provider.CreateScope();
		var db = scope.ServiceProvider.GetRequiredService<StudyMateContext>();
		db.Database.Migrate();
	}
}