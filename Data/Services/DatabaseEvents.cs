namespace StudyMate.Data.Services;
using System;
using Shared.Models;

public class DatabaseEvents
{
	public event Action<Group>? OnGroupCreated;
	public event Action<Group>? OnGroupChanged;

	internal void GroupCreated(Group group)
	{
		OnGroupCreated?.Invoke(group);
	}
	internal void GroupChanged(Group group)
	{
		OnGroupChanged?.Invoke(group);
	}
}
