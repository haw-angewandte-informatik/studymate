namespace StudyMate.Shared.User.Models;
using System;
using Shared.Models;

public class LoginResult
{
	public bool Success { get; set; }

	/// <summary>
	/// Only set if the login was successful
	/// </summary>
	public Guid? Token { get; set; }

	/// <summary>
	/// Only set if the login was successful
	/// </summary>
	public User? UserData { get; set; }

	/// <summary>
	/// Only set if the login failed
	/// </summary>
	public string? ErrorMessage { get; set; }

	/// <summary>
	/// Only set if the login failed and the error is regarding
	/// a specific field of the <see cref="LoginRequest"/>/<see cref="RegistrationRequest"/>
	/// </summary>
	public string? ErrorField { get; set; }
}
