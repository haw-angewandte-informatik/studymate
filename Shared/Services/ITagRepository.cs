namespace StudyMate.Shared.Services;

public interface ITagsRepository
{
	Task<List<string>> AllAsync();
}