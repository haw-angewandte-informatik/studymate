﻿namespace StudyMate.Shared.Models;

public record Group(
	int Id,
	string OwnerUsername,
	string Name,
	uint Capacity,
	ICollection<string> Tags,
	ICollection<User> Members)
{
	public ICollection<User> Members { get; set; } = Members;
	public bool Open => Capacity - Members.Count > 0;
}