namespace StudyMate.Shared.Models;

public class ChatMessage
{
	public static ChatMessage New => new("Anonymous", string.Empty);

	public string Sender { get; set; }
	public string Content { get; set; }

	public ChatMessage(string sender, string content)
	{
		Sender = sender;
		Content = content;
	}

	public override string ToString() => $"{{{Sender}: {Content}}}";
}


