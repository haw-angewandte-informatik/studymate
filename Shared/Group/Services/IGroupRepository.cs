namespace StudyMate.Shared.Group.Services;

using StudyMate.Shared.Models;

public interface IGroupRepository
{
	Task<Group?> FindAsync(int groupId);
	Task<Group> CreateGroup(Shared.Group.Models.NewGroupRequest group);
	Task<List<Group>> GetOpenGroups(int numOfGroups);
	Task<Group> JoinGroup(int groupId, Guid userId);
}