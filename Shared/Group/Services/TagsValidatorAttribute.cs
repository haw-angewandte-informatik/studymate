namespace StudyMate.Shared.Group.Services;

using System.ComponentModel.DataAnnotations;

public class TagsValidatorAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		return GroupDataValidator.AreTagsValid(value as ICollection<string>, out var err)
			? ValidationResult.Success
			: new ValidationResult(err, new[] { validationContext.DisplayName });
	}
}