namespace StudyMate.Shared.Group.Services;

public static class GroupDataValidator
{
	private const int NameMinLength = 3;
	private const int NameMaxLength = 30;

	public static bool IsNameValid(string? name, out string message)
	{
		if (IsEmpty(name))
		{
			message = "Group name can't be empty";
			return false;
		}

		name = name!.Trim();

		if (!NameHasRightLength(name))
		{
			message = $"Group name has to be between {NameMinLength} and {NameMaxLength} characters long";
			return false;
		}

		message = $"{name} is a valid group name";
		return true;
	}

	public static bool AreTagsValid(ICollection<string>? tags, out string message)
	{
		if (EmptyTags(tags))
		{
			message = "Please select at least one tag for your group";
			return false;
		}
		message = "tags of group are valid";
		return true;
	}

	#region [ Name guards ]

	// Username can't be empty
	private static bool IsEmpty(in string? name) => string.IsNullOrEmpty(name);

	// Username has to be between 3 and 30 characters long
	private static bool NameHasRightLength(in string name) => name.Length is >= NameMinLength and <= NameMaxLength;

	#endregion

	#region [ Tags guard ]

	// at least 1 tag
	private static bool EmptyTags(in ICollection<string>? tags) => tags is null || !(tags.Any());

	#endregion
}