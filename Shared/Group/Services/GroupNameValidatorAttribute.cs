namespace StudyMate.Shared.Group.Services;

using System.ComponentModel.DataAnnotations;

public class GroupNameValidatorAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		return GroupDataValidator.IsNameValid(value as string, out var err)
			? ValidationResult.Success
			: new ValidationResult(err, new[] { validationContext.DisplayName });
	}
}