namespace StudyMate.Shared.Group.Services;

using System;
using System.Threading.Tasks;
using Group.Models;
using Shared.Models;

public interface IGroupHub
{
	public const string Route = "hubs/groups";

	/// <summary>
	/// Logs the user in to enable authorized real-time communication.
	/// </summary>
	/// <remarks>Idempotent operation.</remarks>
	/// <param name="token">Login token must be valid.</param>
	/// <returns>Whether or not the login succeeded.</returns>
	Task<bool> Login(Guid token);

	/// <summary>
	/// Logs the user out of authorized real-time communication.
	/// All other communication is still possible, like being notified of new groups.
	/// </summary>
	/// <returns></returns>
	void Logout();

	/// <summary>
	/// Creates a new group. Returns nothing when successful, but triggers an update for all clients.
	/// </summary>
	/// <param name="newGroup"></param>
	/// <returns>A Task finishing after sending the update to all clients.</returns>
	Task CreateGroup(NewGroupRequest newGroup);

	/// <summary>
	/// Join a group. Returns nothing when successful, but triggers an update for all clients.
	/// </summary>
	/// <param name="newGroup"></param>
	/// <returns>A Task finishing after sending the update to all clients.</returns>
	Task JoinGroup(int groupId);

	Task SendMessage(ChatMessage message);
}
