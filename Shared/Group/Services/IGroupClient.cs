namespace StudyMate.Shared.Group.Services;

using Shared.Models;

public interface IGroupClient
{
	public Task OnGroupCreated(Group group);
	public Task OnGroupChanged(Group group);
	public Task ReceiveMessage(ChatMessage message);
}
