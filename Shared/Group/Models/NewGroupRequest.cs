namespace StudyMate.Shared.Group.Models;

using System.ComponentModel.DataAnnotations;
using StudyMate.Shared.Group.Services;
using StudyMate.Shared.Models;

public class NewGroupRequest
{
	public User Owner { get; set; } = null!;
	[GroupNameValidator]
	public string Name { get; set; } = null!;

	[Required]
	[Range(1, 50, ErrorMessage = "Capacity must be between 1 and 50")]
	public int Capacity { get; set; }
	[TagsValidator]
	public List<string> Tags { get; set; } = new();
	public List<User> Members { get; set; } = new();
}