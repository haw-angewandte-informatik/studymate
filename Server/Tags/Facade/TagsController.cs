namespace StudyMate.Server.Tags.Facade;

using Microsoft.AspNetCore.Mvc;
using Shared.Models;
using Shared.Services;

[Route("api/[controller]")]
[ApiController]
public class TagsController : ControllerBase
{
	private readonly ITagsRepository _tagsRepo;

	public TagsController(ITagsRepository tagsRepo)
	{
		_tagsRepo = tagsRepo;
	}

	[HttpGet]
	public async Task<List<string>> AllAsync()
	{
		return await _tagsRepo.AllAsync();
	}
}