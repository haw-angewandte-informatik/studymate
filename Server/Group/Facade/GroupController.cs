namespace StudyMate.Server.Group.Facade;

using Microsoft.AspNetCore.Mvc;
using StudyMate.Server.User.Services;
using StudyMate.Shared.Group.Models;
using StudyMate.Shared.Group.Services;
using StudyMate.Shared.Models;

[Route("api/[controller]")]
[ApiController]
public class GroupController : ControllerBase
{
	private readonly IGroupRepository groupRepo;

	public GroupController(IGroupRepository groupRepo)
	{
		this.groupRepo = groupRepo;
	}

	[HttpGet]
	public async Task<List<Group>> GetOpenGroups([FromQuery] int numOfGroups = 20)
	{
		return await groupRepo.GetOpenGroups(numOfGroups);
	}

	[HttpPost]
	[LoggedIn]
	public async Task CreateGroup(NewGroupRequest request)
	{
		await groupRepo.CreateGroup(request);
	}
}