namespace StudyMate.Server.Group.Facade;

using Microsoft.AspNetCore.SignalR;
using StudyMate.Shared.Group.Models;
using StudyMate.Shared.Group.Services;
using StudyMate.Shared.Models;
using StudyMate.Shared.Services;
using StudyMate.Shared.User.Services;

public class GroupHub : Hub<IGroupClient>, IGroupHub
{
	public static Task NotifyClientsOfNewGroup(IServiceProvider sp, Group group) =>
		sp.GetRequiredService<IHubContext<GroupHub>>().Clients.All.SendAsync(nameof(IGroupClient.OnGroupCreated), group);

	private readonly IUserRepository userRepo;
	private readonly IGroupRepository groupRepo;

	public GroupHub(IUserRepository userRepo, IGroupRepository groupRepo)
	{
		this.userRepo = userRepo;
		this.groupRepo = groupRepo;
	}

	public async Task<bool> Login(Guid token)
	{
		if (await userRepo.IsTokenValid(token))
		{
			Context.Items[UserDataValidator.TokenKey] = token;
			Console.WriteLine($"Logged in using token {token}");
			return true;
		}

		return false;
	}

	public void Logout()
	{
		Context.Items.Remove(UserDataValidator.TokenKey);
	}

	public async Task CreateGroup(NewGroupRequest request)
	{
		if (Context.Items.TryGetValue(UserDataValidator.TokenKey, out var token) && await userRepo.IsTokenValid((Guid)token!))
		{
			var group = await groupRepo.CreateGroup(request);
			OnGroupCreated(group);
		}
	}

	public async Task JoinGroup(int groupId)
	{
		if (Context.Items.TryGetValue(UserDataValidator.TokenKey, out var token) && await userRepo.IsTokenValid((Guid)token!))
		{
			var group = await groupRepo.JoinGroup(groupId, (Guid)token);
			OnGroupChanged(group);
		}
	}

	private void OnGroupCreated(Group group)
	{
		Clients.All.OnGroupCreated(group);
	}

	private void OnGroupChanged(Group group)
	{
		Clients.All.OnGroupChanged(group);
	}

	private async Task<bool> IsLoggedIn() =>
		Context.Items.TryGetValue(UserDataValidator.TokenKey, out var value)
		&& value is Guid token
		&& await userRepo.IsTokenValid(token);

	public async Task SendMessage(ChatMessage message)
	{
		await Clients.All.ReceiveMessage(message);
	}
}
